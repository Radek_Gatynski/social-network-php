<footer class="page-footer font-small blue pt-4">
  <div class="container-fluid text-center text-md-left">
    <div class="row">
      <div class="col-md-8 mt-md-0 mt-8">
        <h5 class="text-uppercase">Social Newtowk more that Facebook</h5>
        <p>You are here to change the perception of the world!</p>
      </div>
      <hr class="clearfix w-100 d-md-none pb-3">
      <div class="col-md-4 mb-md-0 mb-4">
        <h5 class="text-uppercase">Links</h5>
        <ul class="list-unstyled">
          <li>
            <a href="#!">Link 1</a>
          </li>
          <li>
            <a href="#!">Link 2</a>
          </li>
          <li>
            <a href="#!">Link 3</a>
          </li>
          <li>
            <a href="#!">Link 4</a>
          </li>
        </ul>
      </div>
    </div>
    </div>
  <div class="footer-copyright text-center py-3">© <?php echo date("Y");?> Copyright:
    <a href=""> SocialWorld.com</a>
  </div>
</footer>