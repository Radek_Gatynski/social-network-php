<?php

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarToggler">
    <a class="navbar-brand" href="#">SOCIAL Worlds</a>
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        </form>     
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <span class="my-2 my-sm-0"><?php echo $_SESSION['username'];?></span>
      <?php $picture = (isset($_SESSION['picture'])) ? $_SESSION['picture'] : 'http://localhost/website/phpSocialNetwork/assets/images/profilePicture/profileDefaultPix.png'; ?>
      <img class="navbar-user-profile" src="<?php echo $picture; ?>" alt='profile picture' width='35' heigth="38" /> 
      <div>
        <span class="dropdown-toggle navbar-dropdown-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>
        <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="#">Setting</a>
            <a class="dropdown-item" href="./shop.php">Shop</a>
            <a class="dropdown-item" href="#">Change your profile</a>
            <a class="dropdown-item" href="./LoginRegisterLogout/logout.php">Logout</a>
        </div>
      </div>
    </form>
  </div>
</nav>