<?php
if($_SESSION['cookies'] == 0){
    echo "
        <div class='cookie-bar row'>
            <div class='col-lg-9'>
                <h6>This website use cookies</h6>
                <a href='https://en.wikipedia.org/wiki/Magic_cookie' target='_black'>More information on Cookies</a><br />
                <a href='privatePolicy.php' target='_blank'>Please see also our privacy policy in this regard!</a>
            </div>
            <div class='col-lg-3'>
                <button class='btn btn-primary' onclick='acceptCookies(" . $_SESSION['userID'] . ")'>Accept</button>
            </div>
        </div>
    ";
}
?>