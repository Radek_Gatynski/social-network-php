function acceptCookies(item){
    $.post("./handles/ajax/acceptCookies.php",{userID:item})
    .done(function(error){
        if(error!=null){
            $(".cookie-bar").remove();
            return;
        }
       
    })
}


function openPage(url){
    if(timer != null){
        clearTimeout(timer);
    }
    if(url.indexOf("?") == -1){
        url = url + "?";
    }

    var encodedUrl = encodeURI(url);
    $("#mainContent").load(encodedUrl);
    $("body").scrollTop(0);
    history.pushState(null,null,url);
}