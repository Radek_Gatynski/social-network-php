<?php
class ErrorMessages{
    public static $userFirstName = "User fist name should have more that 2 characters and less that 40";
    public static $userLastName = "User fist name should have more that 2 characters and less that 40";

    public static $userEmail = "Email is not valid";
    public static $emailExist = "Email is already in database";
    public static $dontHaveThisAccount = "We don't have user with this information in database";
    public static $country = "Country don't exist in database";
    public static $gender = "Gender don't exist in database";
    public static $passwordsDoNotMatch = "Password and repeat password do not match";
    public static $passwordRegrex = "Password do not match regrex can only contain letters and numbers and have more that 5 characters";
    public static $passwordLength = "Password is to short";
    public static $dbInsertUserError = "Error while saving";
    public static $terms = "Terms and conditions must be checked";
    public static $loginFail = "Error while login";
    public static $sendResetCodeError = "Error when send restart code. Please try again";
    public static $emailDontExist = "We don't have this email in database";
    public static $codeIsNotCorrect = "The code is not correct";
}
?>