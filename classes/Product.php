<?php 
class Product{
    private $conn;
    private $errors;
    private $id;
    private $title;
    private $description;
    private $added;
    private $mysqliData;
    private $picture;
    private $cretedByID;
    private $categories = array();
    public function __construct($conn){
        $this->conn=$conn;
    }

    public function getID(){
        return $this->id;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getDescription(){
        return $this->description;
    }
    public function getPicture(){
        return $this->picture;
    }
    public function getUser(){
        return new User($this->conn,$this->cretedByID);
    }

    public function getCategories(){
        $sql = mysqli_query($this->conn, "Select * from categories");
        if($sql)
            return mysqli_fetch_all($sql);
    }

    public function getProductInformation($id){
        $sql = mysqli_query($this->conn,"Select * from shop where id='$id'");
        if($sql){
            return mysqli_fetch_all($sql, MYSQLI_ASSOC);
        }
    }

    public function getOwnerInformation($id){
        return new User($this->conn,$id);
    }
}
?>