<?php 
class Account{
    private $conn;
    private $errors;
    public function __construct($conn){
        $this->errors = array();
        $this->conn = $conn;
    }
    public function changeUserPassword($email,$password,$passwordRepeat,$code){
        $this->validateEmailCheckIfExist($email);
        $this->validatePassword($password,$passwordRepeat);
        $this->validateCode($code);
        if(empty($this->errors)){
            return $this->changePassword($email,$password,$code);
        }
        else{
            return false;
        }
    }
    private function changePassword($email,$password,$code){
        $pass = md5($password);
        $sql = $this->conn->query("Update users set password='$pass', resetCode=' ' where email='$email' and resetCode='$code'");
        if($sql){
            return true;
        }else{
            return false;
        }
    }
    public function sendCodeToRestartPassword($email){
        $this->validateEmailCheckIfExist($email);
        if(empty($this->errors)){
            return $this->sendCodeRestartPassword($email);
        }else{
            return false;
        }
    }

    public function register($firstN,$lastN,$email,$password,$rPassword,$country,$gender,$terms){
        $this->validateFirstName($firstN);
        $this->validateLastName($lastN);
        $this->validatePassword($password,$rPassword);
        $this->validateEmail($email);
        $this->validateCountry($country);
        $this->validateGender($gender);
        $this->validateTerms($terms);
        if(empty($this->errors)){
            return $this->insertUserDetails($firstN,$lastN,$password,$email,$country,$gender,$terms);
        }else{
            return false;
        }
    }

    public function login($email, $password){
        $pass = md5($password);
        $sql = $this->conn->query("select * from users where email='$email' and password='$pass' and confirmed='1'");
        if($sql->num_rows == 1 ){
            $sql = $sql->fetch_array();
            return $sql;
        }
        else{
            array_push($this->errors, ErrorMessages::$loginFail);
            return false; 
        }
    }


    public function checkCretitals($email,$vkey){
        $email = $this->conn->real_escape_string($email);
        $vkey = $this->conn->real_escape_string($vkey);

        $this->checkVeryficationInfo($email,$vkey);
        if(empty($this->errors)){
            return $this->updateUserInfo($email,$vkey);
        }else{
            return false;
        }
    }

    private function validateCode($code){
        if(!is_numeric($code) && strlen($code)!=6){
            array_push($this->errors, ErrorMessages::$codeIsNotCorrect);
        }
    }

    private function checkVeryficationInfo($email,$vkey){
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            array_push($this->errors, ErrorMessages::$userEmail);
            return;
        }
 
        $checkIfExist = $this->conn->query($this->conn, "Select id from users where email='$email'");
        if($checkIfExist->num_rows == 0){
            array_push($this->errors, ErrorMessages::$dontHaveThisAccount);
        }
    }

    private function updateUserInfo($email,$vkey){
        $trueVAL = "1";
        $sql = $this->conn->query("Update users set confirmed='$trueVAL' where email='$email' AND vkey='$vkey'");
        $check = ($sql)? true: false;
        return $check;
    }

    private function sendCodeRestartPassword($email){
        $code = mt_rand(100000, 999999);
        $results = $this->conn->query("Update users set resetCode='$code' where email='$email'");
        if($results){
            $subject = "Reset password code";
            $message = "To reset password please click this link and enter the code (" . $code . ") <a href='http://localhost/website/phpSocialNetwork/LoginRegisterLogout/resetUserPassword.php?email=$email'>Link</a>";
            $header = "From: radek.gatynski@gmail.com \r\n";
            $header .= "MIME-Version: 1.0" . "\r\n";
            $header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $wasSend = mail($email,$subject,$message,$header);
            return $wasSend;
        }else{
            array_push($this->errors, ErrorsMessages::$sendResetCodeError);
        }
    }

    private function insertUserDetails($firstN,$lastN,$password,$email,$country,$gender,$terms){
        $encryptedPass = md5($password);
        $veryfKEY = md5(time() . $email);
        $results = $this->conn->query("Insert into users values ('','$firstN','$lastN','$encryptedPass','$email','$country','$gender','$terms','','register-system','not-confirmed','','$veryfKEY','','','',0)");
        if($results){
           //HOW SET SENDING EMAIL by mail() : https://stackoverflow.com/questions/15965376/how-to-configure-xampp-to-send-mail-from-localhost
            $subject = "Email Veryfication";
            $message = "Please click to confirm your account <a href='http://localhost/website/phpSocialNetwork/LoginRegisterLogout/confirm-account.php?email=$email&vkey=$veryfKEY'>Link</a>";
            $header = "From: radek.gatynski@gmail.com \r\n";
            $header .= "MIME-Version: 1.0" . "\r\n";
            $header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $wasSend = mail($email,$subject,$message,$header);
            return $wasSend;
        }
        else{
            array_push($this->errors, ErrorMessages::$dbInsertUserError);
        }   
    }

    private function validateFirstName($name){
        if(strlen($name)<2 || strlen($name)>40){
            array_push($this->errors, ErrorMessages::$userFirstName);
            return;
        }
    }
    private function validateLastName($lastName){
        if(strlen($lastName)<2 || strlen($lastName)>40){
            array_push($this->errors, ErrorMessages::$userLastName);
            return;
        }
    }
    private function validateEmail($email){
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            array_push($this->errors, ErrorMessages::$userEmail);
            return;
        }
        $checkEmailExist = $this->conn->query("Select email from users where email='$email'");
        if($checkEmailExist->num_rows == 1){
            array_push($this->errors, ErrorMessages::$emailExist);
        }
    }

    private function validateEmailCheckIfExist($email){
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            array_push($this->errors, ErrorMessages::$userEmail);
            return;
        }
        $checkEmailExist =$this->conn->query("Select email from users where email='$email'");
        if($checkEmailExist->num_rows == 0){
            array_push($this->errors, ErrorMessages::$emailDontExist);
        }
    }

    private function validateCountry($country){
        $checkCountry = $this->conn->query("Select countryCode FROM country where countryCode='$country' Limit 1");
        if($checkCountry->num_rows == 0){
            array_push($this->errors, ErrorMessages::$country);
        }
    }
    private function validateGender($gender){
        $checkGender = $this->conn->query("Select genderCode from gender where genderCode='$gender' Limit 1");
        if($checkGender->num_rows== 0){
            array_push($this->errors, ErrorMessages::$gender);
        }
    }
    private function validatePassword($password, $passwordRepeat){
        if($password != $passwordRepeat){         
            array_push($this->errors, ErrorMessages::$passwordsDoNotMatch);
            return;
        }
        if(preg_match('/[^A-Za-z0-9]/', $password)){
           array_push($this->errors, ErrorMessages::$passwordRegrex);
            return;
        }
        if(strlen($password)>30 || strlen($password)<5){
            array_push($this->errors, ErrorMessages::$passwordLength);
        }
    }

    private function validateTerms($terms){
        if($terms != 1){
            array_push($this->errors,ErrorMessages::$terms);
            return;
        }
    }

    public function getError($msg_error){
        if(!in_array($msg_error, $this->errors)){
            $msg_error = "";
        }
        return "<small class='form-text text-muted'>$msg_error</small>";
    }
}
?>