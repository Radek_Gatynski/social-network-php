<?php
include_once './templates/headerPureHTML.php';
$msg = '';
if(isset($_GET['msg'])){
    $msg = $_GET['msg'];
}
?>

<div class="container-fluid">
    <h1 class="text-center">Some error</h1>
    <p class="text-center"> <?php echo $msg; ?></p>
</div>

<?php 
include_once './templates/footerPureHTML.php';
?>