<?php 
include './templates/DBconfig.php';

if(!isset($_SESSION['username'])){
    header("Location: welcome.php");
    exit();
}
//include './classes/Product.php';
include './templates/header.php';
include './templates/components/navbar.php';
?>
<div class="container-fluid shop-page">
    <header><h3 class="text-center">Shop</h3></header>
    <div class="options">
        <a href="addNewItemToShop.php" class="btn btn-primary">Add new item</a>
    </div>
    <div class="row">
        <?php include './shopLeftNav.php'; ?>
        <div class="items-list col-lg-9">
            <?php include './shopContent.php'; ?>
        </div>
    </div>
</div>
<?php 
include './templates/components/footer.php';
include './templates/footer.php';
?>