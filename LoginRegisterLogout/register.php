<?php
if(isset($_SESSION['username'])){
    header("Location: ../index.php");
    exit();
}
include '../templates/header.php';
include '../classes/Account.php';
include '../classes/ErrorMessages.php';
include_once '../templates/DBconfig.php';
$user = new Account($conn);
include '../handles/register-handles.php';
?>

<div class="login-register-template container-fluid">
    <div class="container">
        <header>
            <h2 class='text-center'>Register new user</h2>
        </header>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
        <form method="POST" action="register.php">
        <?php echo $user->getError(ErrorMessages::$dbInsertUserError); ?>
            <div class="form-group">
                <label for='userFirstName'>First Name:</label>
                <input type='text' value='' class="form-control" id='userFirstName' name='userFirstName' required placeholder='Enter user first name'/>               
                <?php echo $user->getError(ErrorMessages::$userFirstName); ?>     
            </div>
            <div class="form-group">
                <label for='userLastName'>Last name:</label>
                <input type='text' value='' class="form-control" id='userLastName' name='userLastName' required placeholder='Enter user last name'/>
                <?php echo $user->getError(ErrorMessages::$userLastName); ?>     
            </div>
            <div class="form-group">
                <label for='email'>Email:</label>
                <input type='email' value='' class="form-control" id='email' name='email' required placeholder='Enter email'/>
                <?php echo $user->getError(ErrorMessages::$userEmail); ?>     
            </div>
            <div class="form-group">
                <label for='password'>Password:</label>
                <input type='password' value='' class="form-control" id='password' name='password' required placeholder='Enter password'/>
                <?php echo $user->getError(ErrorMessages::$passwordRegrex); ?>     
                <?php echo $user->getError(ErrorMessages::$passwordLength); ?>     
            </div>
            <div class="form-group">
                <label for='passwordRepeat'>Repeat password:</label>
                <input type='password' value='' class="form-control" id='passwordRepeat' name='passwordRepeat' required placeholder='Repeat password'/>
                <?php echo $user->getError(ErrorMessages::$passwordsDoNotMatch); ?>
            </div>
            <div class="form-group">
                <label for='country'>Country:</label>
                <select class="form-control"  name="country" required>
                    <option value="en" selected="selected">England</option>
                    <option value="pl">Poland</option>
                    <option value="us">US</option>
                    <option value="prc">China</option>
                    <option value="ge">German</option>
                </select>
                <?php echo $user->getError(ErrorMessages::$country); ?>
            </div>
            <div class="form-group">
                <label for='country'>Gender:</label>
                <select class="form-control"  name="gender" required>
                    <option value="men">Men</option>
                    <option value="women">Women</option>
                    <option value="none" selected="selected">None</option>
                </select>
                <?php echo $user->getError(ErrorMessages::$gender); ?>
            </div>
            <div class="form-check">
                <input type="checkbox" name="terms" class="form-check-input" id="terms" value="1" required>
                <label class="form-check-label" for="terms"><a href="../terms.html">Accept terms and conditions</a></label>
                <?php echo $user->getError(ErrorMessages::$terms); ?>
            </div>
            <input type="submit" name="register" class="btn btn-primary" value="Sign up" />
        </form>
            <a href="login.php" class="text-muted text-center">Already have an account?</a><br />
        </div>
        </div>
    </div>
</div>