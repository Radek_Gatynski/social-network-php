<?php 
include '../templates/header.php';
include '../classes/Account.php';
include '../classes/ErrorMessages.php';
include_once '../templates/DBconfig.php';
$user = new Account($conn);
include '../handles/forgot-password-handles.php';
?>
<div class="container-fluid">
    <header>
        <h1 class="text-center">Reset password</h1>
    </header>
    <div class="row">
            <div class="col-lg-6 offset-lg-3">
        <form method="POST" action="forgotPassword.php">
        <?php echo $user->getError(ErrorMessages::$userEmail); ?>
        <?php echo $user->getError(ErrorMessages::$sendResetCodeError); ?>
            <div class="form-group">
                <label for='email'>Email address:</label>
                <input type='email' value='' class="form-control" id='email' name='email' required placeholder='Enter your email address'/>               
                <?php echo $user->getError(ErrorMessages::$emailDontExist); ?>     
            </div>
            <input type="submit" name="send-restart-code" value="Send code" class="btn btn-primary" />
        </form>
</div>
