<?php 
if(isset($_SESSION['username'])){
    header("Location: index.php");
    exit();
}

require_once './configGoogle.php'; 
if(isset($_GET['code'])){
    $token = $gClient->fetchAccessTokenWithAuthCode($_GET['code']);
    $_SESSION['access_token'] = $token;
}else{
    header("location: register.php");
    exit();
}

require './templates/DBconfig.php';
$oAuth = new Google_Service_Oauth2($gClient);
$userData = $oAuth->userinfo_v2_me->get();
if($userData['name']){
   $_SESSION['username'] = $userData['name'];
   //$_SESSION['username'] = "Majtek";

    $_SESSION['gender'] = $userData['gender'];
    $_SESSION['country'] = $userData['locate'];
    $_SESSION['email'] = $userData['email'];
    $_SESSION['picture'] = $userData['picture'];
    $_SESSION['profileFROM'] = 'google';
    header("Location: index.php");
}else{
    header("Location: someError.php?msg='Error while try login with Google Account'");
    exit();
}
?>