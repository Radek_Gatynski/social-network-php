<?php
if(isset($_SESSION['username'])){
    header("Location: ../index.php");
    exit();
}
include '../templates/header.php';
include_once '../templates/DBconfig.php';
include '../classes/Account.php';
include '../classes/ErrorMessages.php';
$user = new Account($conn);
include '../handles/login-handles.php';
require_once './configGoogle.php';
$loginURL = $gClient->createAuthUrl();
?>

<div class="login-register-template container-fluid">
    <div class="container">
        <header>
            <h2 class='text-center'>Login</h2>
        </header>
        
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <form method="POST" action="login.php">
                <p class="text-center text-danger"><?php echo $user->getError(ErrorMessages::$loginFail);?></p>
                    <div class="form-group">
                        <label for='email'>Email:</label>
                        <input type='email' value='' class="form-control" id='email' name='email' required placeholder='Enter email'/>        
                    </div>
                    <div class="form-group">
                        <label for='password'>Password:</label>
                        <input type='password' value='' class="form-control" id='password' name='password' required placeholder='Enter password'/>
                    </div>
                    <input type="submit" name="login" class="btn btn-primary" value="Login" />
                </form>
                    <a href="login.php" class="text-muted text-center">Don't have an account?</a><br />
                    <a href="forgotPassword.php" class="text-center">Don't remember password</a><br />
                    <input onclick="window.location.href = '<?php echo $loginURL ?>';" class="btn btn-danger google" value="Login using Google Account">
            </div>
        </div>
    </div>
</div>