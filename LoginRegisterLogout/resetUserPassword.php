<?php
include '../templates/header.php';
include_once '../templates/DBconfig.php';
include '../classes/Account.php';
include '../classes/ErrorMessages.php';
$user = new Account($conn);
include '../handles/change-password-handles.php';
?>

<div class="login-register-template container-fluid">
    <div class="container">
        <header>
            <h2 class='text-center'>Change password</h2>
        </header>
        
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <form method="POST" action="resetUserPassword.php">
                    <div class="form-group">
                        <label for='Email'>Email:</label>
                        <input type='Email' value='<?php echo $email;?>' class="form-control" id='email' name='email' required readonly/>        
                    </div>
                    <div class="form-group">
                        <label for='password'>Password:</label>
                        <input type='password' value='' class="form-control" id='password' name='password' required placeholder='Enter password'/>        
                    </div>
                    <div class="form-group">
                        <label for='password'>Repeat password:</label>
                        <input type='password' value='' class="form-control" id='passwordRepeat' name='passwordRepeat' required placeholder='Enter repeat password'/>
                    </div>
                    <div class="form-group">
                        <label for="code">Code:</label>
                        <input type="number" value='' class="form-control" id="code" name="code" required placeholder="Enter code from email: " />
                    </div>
                    <input type="submit" name="reset" class="btn btn-primary" value="Reset" />
                </form>
             </div>
        </div>
    </div>
</div>