<div class="filters col-lg-3">
            <h4 class="text-center">Filters</h4>
            <div class="search-by-title">
                <input type='text' class="form-control" placeholder="Enter item name..." ?>
            </div>
            <div class="price">
                <h6>Price</h6>
            </div>
            <div class="tags">
                <h6 class="text-center">By Tags</h6>
                <button class="btn btn-primary">Toys</button>
                <button class="btn btn-secondary">Cars</button>
                <button class="btn btn-success">Clothes</button>
                <button class="btn btn-danger">Tech</button>      
                <button class="btn btn-warning">Food</button>
                <button class="btn btn-info">Tickets</button>
                <button class="btn btn-light">Trips</button>
                <button class="btn btn-dark">Others</button>
            </div>
            <div class="category">
                <h6 class="text-center">Category</h6>
                <?php include './productCategoriesList.php'; ?>
            </div>
</div>