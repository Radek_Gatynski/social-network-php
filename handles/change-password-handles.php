<?php
$email = '';
if(isset($_GET['email'])){
  $email = $_GET['email'];
}
if(isset($_POST['reset'])){
  
    $email = $conn->real_escape_string($_POST['email']);
    $password = $conn->real_escape_string($_POST['password']);
    $passwordRepeat = $conn->real_escape_string($_POST['passwordRepeat']);
    $code = $conn->real_escape_string($_POST['code']);

    $result = $user->changeUserPassword($email,$password,$passwordRepeat,$code);
    if($result){
        header("Location: login.php");
    }else{
        header("location: ../infopage.php?msg='Something went wrong try again'");
    }
}
?>