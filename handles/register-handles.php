<?php
if(isset($_SESSION['username'])){
    header("Location: ../index.php");
    exit();
}

if(isset($_POST["register"])){
    $userFirstName = $conn->real_escape_string($_POST['userFirstName']);
    $userLastName = $conn->real_escape_string($_POST['userLastName']);
    $password = $conn->real_escape_string($_POST["password"]);
    $passwordRepeat = $conn->real_escape_string($_POST['passwordRepeat']);
    $email = $conn->real_escape_string($_POST['email']);
    $country = $conn->real_escape_string($_POST['country']);
    $gender = $conn->real_escape_string($_POST['gender']);
    $terms = $conn->real_escape_string($_POST['terms']);

    $results = $user->register($userFirstName,$userLastName,$email,$password,$passwordRepeat,$country,$gender,$terms);
    if($results){
        header("Location: ../LoginRegisterLogout/emailSend.php");
    }else{
        header('Location: ../someError.php');
    }
}
?>