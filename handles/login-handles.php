<?php 
if(isset($_SESSION['username'])){
    header("Location: ../index.php");
    exit();
}

if(isset($_POST["login"])){
    $email = $conn->real_escape_string($_POST['email']);
    $password = $conn->real_escape_string($_POST["password"]);
    $result = $user->login($email,$password);
    if($result){
        $_SESSION['username'] = $result['firstName'] . " " . $result["LastName"];
        $_SESSION['email'] = $result['email'];
        $_SESSION['gender'] = $result['gender'];
        $_SESSION['country'] = $result['country'];
        $_SESSION['profileFrom'] = $result['profileFrom'];
        $_SESSION['onlineStatus'] = 'active';
        $_SESSION['userID'] = $result['id'];
        $_SESSION['cookies'] = $result['cookies'];
        header("Location: ../index.php");
    }
}
?>