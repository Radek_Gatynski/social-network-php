<?php 
if(isset($_POST['send-restart-code'])){
    $email = $conn->real_escape_string($_POST['email']);
    $result = $user->sendCodeToRestartPassword($email);
    if($result){
        header("location: ../infopage.php?msg='The mail with reset code was send, please check your email account'");
    }else{
        header("location: ../infopage.php?msg='Something went wrong!'");
    }
}
?>